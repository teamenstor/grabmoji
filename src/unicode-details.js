const table_emojiDetail = $("table.emoji-detail");
const codepoints = getCodepoints(table_emojiDetail.find("td:contains('Codepoints')+td").text().split(", "));
const shortcode = getShortcode(table_emojiDetail.find("td:contains('Shortcodes')+td").text().split(", "));

if (shortcode.length) {
	const svgAnchor = getDownloadAnchor('svg', codepoints, shortcode);
	const pngAnchor = getDownloadAnchor('png', codepoints, shortcode);

	$("<tr>")
		.append("<td>Twemoji</td>")
		.append($("<td>").append(svgAnchor).append("&nbsp;&nbsp;").append(pngAnchor))
		.insertAfter("table.emoji-detail tr:contains('Codepoints')");
}