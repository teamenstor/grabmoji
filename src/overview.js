const section_vendorList = $("section.vendor-list");

const section_codepoints = $(`<section class="codepoints">`);
{
	const h2 = $("h2:contains('Codepoints')");
	section_codepoints.append(h2, h2.next());
}
const section_shortcodes = $(`<section class="shortcodes">`);
{
	const h2 = $("h2:contains('Shortcodes')");
	section_shortcodes.append(h2, h2.next());
}
section_codepoints.insertBefore(section_vendorList);
section_shortcodes.insertBefore(section_vendorList);

const li_twitterVendor = section_vendorList.find("li:contains('Twitter')");

if (li_twitterVendor.length) {
	li_twitterVendor.prependTo(li_twitterVendor.parent());

	const codepoints = getCodepoints(section_codepoints.find("li>a"), true);
	const shortcode = getShortcode(section_shortcodes.find(".shortcode"), true);

	const svgAnchor = getDownloadAnchor('svg', codepoints, shortcode);
	const pngAnchor = getDownloadAnchor('png', codepoints, shortcode);

	li_twitterVendor.find(".vendor-info:first")
		.append($("<p>Download as&nbsp;&nbsp;</p>").append(svgAnchor).append("&nbsp;&nbsp;").append(pngAnchor));
}

