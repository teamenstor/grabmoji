String.prototype.substringFrom = function (str) { return this.substring(this.lastIndexOf(str) + 1); }

function getCodepoints(elements, isJQuery) {
	let output = [];
	for (let el of elements) {
		let text = isJQuery ? $(el).text() : el;
		output.push(text.substringFrom('+').toLowerCase());
	}
	if (output.length == 2 && output[1] == "fe0f")
		output.pop();
	return output;
}

function getShortcode(elements, isJQuery) {
	let output;
	if (isJQuery) {
		output = elements.last().text();
	} else {
		output = elements.length ? elements[elements.length - 1] : '';
	}
	if (output.length >= 2) {
		output = output.substring(1, output.length - 1);
	}
	return output;
}

function convertToBlob(element) {
	const $element = $(element);
	const href = $element.attr('href');
	fetch(href)
		.then(resp => resp.blob())
		.then(blob => {
			const blobUrl = window.URL.createObjectURL(blob);
			$element
				.attr('external', href)
				.attr('href', blobUrl);
		})
		.catch(err => console.error("GrabMoji error: ", err));
}

function getDownloadAnchor(type, codepoints, shortcode) {
	const id = codepoints.join('-');

	let link;
	let title;
	switch (type.toLowerCase()) {
		case 'svg':
			link = `https://twemoji.maxcdn.com/v/latest/svg/${id}.svg`;
			title = "SVG";
			break;
		case 'png':
			link = `https://twemoji.maxcdn.com/v/latest/72x72/${id}.png`;
			title = "PNG";
			break;
		default:
			return $();
	}
	let output = $(`<a href="${link}" target="_blank" download="${shortcode}">${title}</a>`);
	convertToBlob(output);
	return output;
}