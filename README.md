## GrabMoji: Emojipedia for Developers and Designers

**Develop and design with emoji!** GrabMoji lets you easily download Twemoji directly from Emojipedia in both SVG and PNG formats, and rearranges information in order to improve the Emojipedia experience for developers.

---

### Features

- **Easily download Twemoji directly from Emojipedia!** GrabMoji will add download links for both SVG and PNG from [MaxCDN](https://www.maxcdn.com/). The Twitter listing will also be moved to the top of the list, so it's easier to find.
- **Find codepoints and shortcodes for emoji quicker!** GrabMoji rearranges the Emojipedia page so that codepoints and shortcodes are found near the top. Much more accessible!

![Image showing how the Emojipedia page for the turtle emoji (🐢) looks with and without GrabMoji](https://files.teamenstor.com/assets/grabmoji/comparison.png "Comparison")

### Installation

*At this moment, GrabMoji is not available in any web store. **Enable various developer modes at your own risk!***

Download the zipped extension [here](grabmoji.zip).
#### Chrome, Opera, Brave (and other [Chromium-based browsers](https://en.wikipedia.org/wiki/Chromium_(web_browser)#Browsers_based_on_Chromium))
1. Unzip the extension.
2. Go to `chrome://extensions` and enable the *Developer mode*.
3. Click *Load unpacked* and locate the root of the unzipped extension (the directory containing `manifest.json`).
4. Done!

#### Firefox
1. Go to `about:debugging` and navigate to *This Firefox*.
2. Click *Load Temporary Add-on* and locate the extension *.zip* file.
4. Done!

#### Edge (EdgeHTML)
1. Unzip the extension.
2. Go to `about:flags` and enable the *Enable extension developer features* flag.
3. Go to the *Extensions* pane.
4. Click *Load extension* and locate the root of the unzipped extension (the directory containing `manifest.json`).
5. Done!

## Bugs
If you discover any bugs or issues with GrabMoji, please add a [GitLab issue](https://gitlab.com/teamenstor/grabmoji/-/issues/new).

## Contributing
I do not currently accept code contributions for GrabMoji. However, if you'd like to request a feature, please add a [GitLab issue](https://gitlab.com/teamenstor/grabmoji/-/issues/new) describing your idea.

---

*I am not in any way associated with Emojipedia, MaxCDN, Twitter, or the Twemoji project. This is solely a personal project that I made for my own private use.*