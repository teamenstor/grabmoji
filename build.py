import zipfile;
import glob;
import os;
import re;

src_files = [f for f in glob.glob("src/**", recursive=True) if os.path.isfile(f)]

with zipfile.ZipFile("grabmoji.zip", 'w') as zip:
	for f in src_files:
		if f.endswith(".js") and not f.endswith(".min.js") and (re.sub(r"\.js$", ".min.js", f) in src_files):
			continue
		a_name = f.split(os.sep, 1)[-1]
		zip.write(f, a_name)
